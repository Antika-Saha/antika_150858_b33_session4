
<?php

$text = "\t\tThese are a few words :) ...  ";
$binary = "   \x09Example string\x0A";
$hello  = "   Hello World";

var_dump($text, $binary, $hello);
echo "<br>";
echo "<br>";

$trimmed = ltrim($text);
var_dump($trimmed);
echo "<br>";

$trimmed = ltrim($text, " \t.");
var_dump($trimmed);
echo "<br>";

$trimmed = ltrim($hello, "Hdle");
var_dump($trimmed);

?>